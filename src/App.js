import React, { Component } from 'react';
import './App.css';
import HeaderComponent from './components/header';

class App extends Component {
  render() {
    return (
      <div>
        <HeaderComponent />
      </div>
    );
  }
}

export default App;
