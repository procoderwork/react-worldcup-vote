import React, { Component } from 'react'

export default class HeaderComponent extends Component {
  render() {
    return (
        <div className="header">
        <div className="header-inner">
          <div className="menu-wrapper">
            <ul className="menu">
              <li className="logo mobile-logo">
                <a href="#">
                  <img src="./1_files/cluster.png" alt="LOGO" />
                </a>
              </li>
            </ul>
          </div>
          <div className="logo-wrapper">
            <div className="logo">
              <a href="">
                <img src="./1_files/cluster.png" alt="LOGO" />
              </a>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
